package com.integrador.proyecto.cuartoa;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity {
Button btnlogin, btningresar, btnbuscar, btnParametro, practica4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        btnlogin = (Button)findViewById(R.id.btnlogin);
        btningresar = (Button)findViewById(R.id.btningresar);
        btnbuscar = (Button)findViewById(R.id.btnbuscar);
        btnParametro = (Button)findViewById(R.id.btnPasarParametro);
        practica4 = (Button)findViewById(R.id.practica4);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadLogin.class);
                        startActivity(intent);
            }
        });

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadRegistrar.class);
                startActivity(intent);
            }
        });

        btnbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadBuscar.class);
                startActivity(intent);
            }
        });

        btnParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadPasarParametro.class);
                startActivity(intent);
            }
        });
        practica4.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadFragmento.class);
                startActivity(intent);
            }
        });
        }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                intent = new Intent(ActividadPrincipal.this, ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(ActividadPrincipal.this, ActividadRegistrar.class);
                break;
            case R.id.loginDialogo:
               Dialog dialogoLoginz = new Dialog(ActividadPrincipal.this);
               dialogoLoginz.setContentView(R.layout.dlg_login);

               Button botonAutenticar = (Button) dialogoLoginz.findViewById(R.id.btnAutenticarz);
               final EditText cajausuarioz = (EditText) dialogoLoginz.findViewById(R.id.txtUser);
               final EditText cajaclavez = (EditText) dialogoLoginz.findViewById(R.id.txtPassword);

               botonAutenticar.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       Toast.makeText(ActividadPrincipal.this, cajausuarioz.getText().toString() + " " + cajaclavez.getText().toString(), Toast.LENGTH_LONG).show();
                   }
               });

               dialogoLoginz.show();
                        break;
        }
        return true;
    }
}
